Source: golang-doozer
Section: devel
Priority: extra
Maintainer: Tonnerre Lombard <tonnerre@ancient-solutions.com>
Build-Depends: debhelper (>= 9), golang-go, golang-pretty-dev,
 golang-goprotobuf-dev, golang-text-dev, dh-golang
Standards-Version: 3.9.4
Homepage: https://github.com/4ad/doozer
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-doozer.git
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-doozer

Package: golang-doozer-dev
Architecture: all
Depends: ${shlibs:Depends}, ${misc:Depends}, golang-pretty-dev,
 golang-goprotobuf-dev
Description: Go client driver for doozerd, a consistent, distributed data store
 This package contains the client library for developing Doozer lock service
 clients. It allows programs to perform all operations the Doozer lock
 service implements, such as:
 .
   * Instant distribution of configurations or settings,
   * Implementing instantly-updated namespaces,
   * Master election
 .
 and many others.

Package: golang-doozer-bin
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Go client for doozerd, a consistent, distributed data store
 This package contains the doozer binary for interacting with the Doozer
 server. It implements all of the operations supported by the Doozer
 client library, but can be used on the command line.
